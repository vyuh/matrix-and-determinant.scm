# Concepts of Matrices and Determinants Expressed in MIT/GNU Scheme
## Usage
If you do not have MIT/GNU Scheme on your system
get it running by following instructions given at
https://www.gnu.org/software/mit-scheme/.
(I found that the program works with [`tinyscheme`](http://tinyscheme.sourceforge.net/download.html) too.)
Then, download and extract an
[archive of this Repository](https://gitlab.com/vyuh/matrix-and-determinant.scm/repository/archive.zip?ref=master).
In a Terminal Window,
Change Working Directory
to the extarcted archive,
then,
you can run commands similar to the following:

    $ rlwrap ~/path/to/mit-scheme-c-9.2/bin/mit-scheme --load matdet.scm
    MIT/GNU Scheme running under GNU/Linux
    .
    .
    .
    ;Loading "matdet.scm"... done
    
    1 ]=> (let
            ((a (make-square-matrix
                    4
                    (list
                        4 3 47 3
                        4 5 6 7
                        7 7 7 5
                        3 2 56 46))))
            (matrix-multiply a (square-matrix-inverse a)))
    
    ;Value 2: (4 4 (1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1))
    
    1 ]=> 
    End of input stream reached.
    Moriturus te saluto.
    $ 

Note that [`rlwrap`](https://linux.die.net/man/1/rlwrap)
can be ommited safely,
it is only to make keyboard entry more conveninent.

## Functions Available

- make-matrix
- matrix-number-of-rows
- matrix-number-of-columns
- matrix-elements
- matrix-element
- make-square-matrix
- square-matrix-minor
- square-matrix-co-factor
- square-matrix-determinant
- square-matrix-adjoint
- square-matrix-inverse
- matrix-remove-row-column
- matrix-multiply-scalar
- matrix-multiply
