(define make-matrix 
  (lambda (m n elems)
      (if
        (= (length elems) (* m n))
        (list m n elems)
        ())))
(define (matrix-number-of-rows matrix)
  (car matrix))
(define (matrix-number-of-columns matrix)
  (cadr matrix))
(define (matrix-elements matrix)
  (caddr matrix))
(define make-square-matrix
  (lambda (n elems)
      (make-matrix n n elems)))

(define matrix-element
  (lambda (matrix i j)
    (if
      (and 
        (<= i (matrix-number-of-rows matrix))
        (<= j (matrix-number-of-columns matrix)))
      (list-ref
        (matrix-elements matrix)
        (+
          (* (- i 1) (matrix-number-of-columns matrix))
          (- j 1)))
      ())))
(define (square-matrix-minor square-matrix i j)
  (if
    (and
      (=
        (matrix-number-of-rows square-matrix)
        (matrix-number-of-rows square-matrix))
      (<= i (matrix-number-of-rows square-matrix))
      (<= j (matrix-number-of-columns square-matrix)))
    (square-matrix-determinant
      (matrix-remove-row-column square-matrix i j))
    ()))
(define (matrix-remove-row-column matrix row column)
  (if
    (and
      (<= row (matrix-number-of-rows matrix))
      (<= column (matrix-number-of-columns matrix)))
    (make-matrix
      (- (matrix-number-of-rows matrix) 1)
      (- (matrix-number-of-columns matrix) 1)
      (do
        ((i 1 (+ i 1))
         (out () (if (= i row) out (append out (do ( (j 1 (+ j 1)) (r () (if (= j column) r (append r (list (matrix-element matrix i j))))))
                 ((> j (matrix-number-of-columns matrix)) r))))))
        ((> i (matrix-number-of-rows matrix)) out)))
    ()))
(define (square-matrix-determinant square-matrix)
  (if
    (and
      (=
        (matrix-number-of-rows square-matrix)
        (matrix-number-of-rows square-matrix)))
    (if
      (= 1 (matrix-number-of-rows square-matrix))
      (matrix-element square-matrix 1 1)
      (if
        (= 2 (matrix-number-of-rows square-matrix))
        (- 
          (*
            (matrix-element square-matrix 1 1)
            (matrix-element square-matrix 2 2))
          (*
            (matrix-element square-matrix 2 1)
            (matrix-element square-matrix 1 2)))
        (do ((i 1 (+ 1 i)) (det 0 (+ det (* (matrix-element square-matrix 1 i) (square-matrix-co-factor square-matrix 1 i)))))
          ((> i (matrix-number-of-columns square-matrix)) det))))
    ()))
(define (square-matrix-co-factor square-matrix i j)
  (if
    (and
      (=
        (matrix-number-of-rows square-matrix)
        (matrix-number-of-rows square-matrix))
      (<= i (matrix-number-of-rows square-matrix))
      (<= j (matrix-number-of-columns square-matrix)))
    (if
      (odd? (+ i j))
      (- (square-matrix-minor square-matrix i j))
      (square-matrix-minor square-matrix i j))
    ()))
(define (square-matrix-inverse square-matrix)
  (if
    (and
      (=
        (matrix-number-of-rows square-matrix)
        (matrix-number-of-rows square-matrix)))
    (let
      ((det (square-matrix-determinant square-matrix)))
      (if
        (zero? det)
        ()
        (matrix-multiply-scalar (square-matrix-adjoint square-matrix) (/ 1 det))))
    ()))
(define (matrix-multiply-scalar matrix scalar)
  (make-matrix
    (matrix-number-of-rows matrix)
    (matrix-number-of-columns matrix)
    (map 
      (lambda (x) (* scalar x))
      (matrix-elements matrix))))
(define (square-matrix-adjoint square-matrix)
   (if
    (and
      (=
        (matrix-number-of-rows square-matrix)
        (matrix-number-of-rows square-matrix)))
    (make-matrix
      (matrix-number-of-rows square-matrix)
      (matrix-number-of-columns square-matrix)
      (do
        ((i 1 (+ i 1))
         (out () (append out (do ( (j 1 (+ j 1)) (r () (append r (list (square-matrix-co-factor square-matrix j i)))))
                 ((> j (matrix-number-of-columns square-matrix)) r)))))
        ((> i (matrix-number-of-rows square-matrix)) out)))
    ()))
(define (matrix-multiply A B)
   (if
     (= (matrix-number-of-rows B) (matrix-number-of-columns A))
     (make-matrix
       (matrix-number-of-rows A)
       (matrix-number-of-columns B)
       (do
         (
          (i 1 (+ i 1))
          (out
            ()
            (append
              out
              (do
                (
                 (j 1 (+ j 1))
                 (r
                   ()
                   (append
                     r
                     (list
                       (do
                         (
                          (k 1 (+ k 1))
                          (s
                            0
                            (+
                              s
                              (*
                                (matrix-element A i k)
                                (matrix-element B k j)))))
                          ((> k (matrix-number-of-columns A)) s))))))
                ((> j (matrix-number-of-columns B)) r)))))
         ((> i (matrix-number-of-rows A)) out)))
     ()))

